#!/bin/sh
##
## clean.sh for clean in /home/vincent/USE/codegen_plugin
##
## Made by
## <vincent.davoust@gmail.com>
##
## Started on  Mon May 14 15:35:14 2018
## Last update Tue Jun 12 10:32:57 2018 
##



rm org/tzi/use/xmi/*.class
rm org/tzi/use/gui/plugins/xmi/*.class

for d in ./org/tzi/use/xmi/*/ ; do
    rm $d*.class
done;
