#!/bin/sh
##
## build.sh for build in /home/vincent/USE/codegen_plugin
##
## Made by
## <vincent.davoust@gmail.com>
##
## Started on  Mon May  7 14:03:53 2018
## Last update Tue Jun 12 10:37:37 2018 
##

usePath=/home/vincent/USE/use-5.0.1/

if [[ $# -eq 1 ]]
then
    usePath=$1
else
    echo "Usage : %> ./build.sh [PATH_TO_USE_INSTALL]"
    echo ""
    echo "Ex : %> ./build.use /opt/use-5.0.1/"
fi

cd org/tzi/use/xmi
echo "Building "$(pwd)
javac -cp ${usePath}/lib/use.jar *.java
#for d in ./*/ ; do
#    if [[ $d != *Example* ]]
#       then
#	   cd $d ;
#	   echo "Building "$(pwd)
#	   javac -cp ${usePath}/lib/use.jar:../../../../../ *.java ;
#	   cd .. ;
#    fi
#done;

cd ../../../../
cd org/tzi/use/gui/plugins/xmi
echo "Building "$(pwd)
javac -cp ${usePath}/lib/use.jar:../../../../../../ *.java
cd ../../../../../../

echo "Making jar"
jar cfm XMI_plugin.jar Manifest.txt resources org useplugin.xml

echo "Installing jar"
cp ./XMI_plugin.jar ${usePath}/lib/plugins/
