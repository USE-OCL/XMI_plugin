package org.tzi.use.gui.plugins.xmi;

import org.tzi.use.runtime.IPlugin;
import org.tzi.use.runtime.IPluginRuntime;
import org.tzi.use.runtime.impl.Plugin;

public class XmiPlugin extends Plugin implements IPlugin {

    final protected String PLUGIN_ID = "XmiPlugin";

    public String getName() {
	return this.PLUGIN_ID;
    }

    public void run(IPluginRuntime pluginRuntime) throws Exception {

    }

}
