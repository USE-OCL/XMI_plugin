# README #

## Goal ##

The goal of this plugin is to add fuctionality to import/export XMI from/to USE


## Usage ##
* move the jar file `XMI_plugin.jar` to the ./lib/plugins/ folder of your USE install
* Run USE

* Click Plugins -> XMI import/export -> Import XMI
or
* Click Plugins -> XMI import/export -> Export XMI